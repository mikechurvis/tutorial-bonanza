﻿using System;
using System.Collections.Generic;

namespace Graphing
{
    public class SampleGraph<TNode> : IGraph<TNode>
    {
        HashSet<TNode> nodeSet;
        Dictionary<TNode, HashSet<TNode>> edgeDict;



        public SampleGraph ()
        {
            nodeSet = new HashSet<TNode>();
            edgeDict = new Dictionary<TNode, HashSet<TNode>>();
        }



        public int NodeCount
        {
            get
            {
                return nodeSet.Count;
            }
        }

        public int EdgeCount
        {
            get
            {
                int count = 0;

                foreach (TNode node in edgeDict.Keys)
                {
                    count += edgeDict[node].Count;
                }

                return count;
            }
        }



        public void AddNode(TNode node)
        {
            if (node == null)
                throw new ArgumentNullException("Attempted to add null node to graph");

            nodeSet.Add(node);
        }

        public void AddEdge(TNode node1, TNode node2)
        {
            if (node1 == null)
                throw new ArgumentNullException("Attempted to add edge from null node");
            if (node2 == null)
                throw new ArgumentNullException("Attempted to add edge to null node");

            if (!edgeDict.ContainsKey(node1) || edgeDict[node1] == null)
                edgeDict.Add(node1, new HashSet<TNode>());

            edgeDict[node1].Add(node2);
        }



        public bool RemoveNode(TNode node)
        {
            if (node == null)
                return false;

            Isolate(node);

            return nodeSet.Remove(node);
        }

        public bool RemoveEdge(TNode node1, TNode node2)
        {
            if (edgeDict.ContainsKey(node1))
                return false;
            else
                return edgeDict[node1].Remove(node2);
        }

        public bool Isolate(TNode node)
        {
            bool edgesRemoved = false;

            if (edgeDict.ContainsKey(node))
            {
                if (edgeDict[node].Count > 0)
                    edgesRemoved = true;

                edgeDict.Remove(node);
            }

            foreach (TNode source in edgeDict.Keys)
            {
                if (edgeDict[source].Remove(node))
                {
                    edgesRemoved = true;
                }
            }

            return edgesRemoved;                    
        }

        public void IsolateAll()
        {
            foreach (TNode node in nodeSet)
                Isolate(node);
        }



        public bool ContainsNode(TNode node)
        {
            return nodeSet.Contains(node);
        }

        public bool ContainsEdge(TNode node1, TNode node2)
        {
            if (!edgeDict.ContainsKey(node1))
                return false;
            else
                return edgeDict[node1].Contains(node2);
        }

        public int EdgeCountFrom(TNode node)
        {
            if (!edgeDict.ContainsKey(node))
                return 0;
            else
                return edgeDict[node].Count;
        }

        public int EdgeCountTo(TNode node)
        {
            int count = 0;
            foreach (TNode source in edgeDict.Keys)
            {
                count += (edgeDict[source].Contains(node)) ? 1 : 0;
            }

            return count;
        }



        public void Clear()
        {
            edgeDict.Clear();
            nodeSet.Clear();
        }

        public void Cleanse()
        {
            throw new NotImplementedException();
        }
    }
}
