﻿namespace Graphing
{
    public interface IGraph<TNode>
    {
        int NodeCount { get; }
        int EdgeCount { get; }

        void AddNode(TNode node);
        void AddEdge(TNode node1, TNode node2);

        bool RemoveNode(TNode node);
        bool RemoveEdge(TNode node1, TNode node2);
        bool Isolate(TNode node);
        void IsolateAll();

        bool ContainsNode(TNode node);
        bool ContainsEdge(TNode node1, TNode node2);
        int EdgeCountFrom(TNode node);
        int EdgeCountTo(TNode node);

        void Clear();
        void Cleanse();
    }
}